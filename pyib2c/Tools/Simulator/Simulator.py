import random
import math
import numpy as np

import pygame

import pymunk
from pymunk.pygame_util import DrawOptions
from pymunk.vec2d import Vec2d

# Global variables
WIDTH = 800
HEIGHT = 600
ROBOT_SIZE = 40
MIN_OBSTACLES = 0
MAX_OBSTACLES = 1
MIN_OBSTACLE_SIZE = 10
MAX_OBSTACLE_SIZE = 100
MIN_OBSTACLE_SPEED = 1
MAX_OBSTACLE_SPEED = 5
TARGETS = 3
THRESHOLD = 100
PAUSE = 100

# Initialize pygame

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()

# Turn off alpha
screen.set_alpha(None)

# Showing sensors and redrawing slows things down.
show_sensors = True
draw_screen = True


class GameState:
    def __init__(self):
        # Global
        self.crashed = False

        # Physics
        self.space = pymunk.Space()
        self.space.gravity = pymunk.Vec2d(0.0, 0.0)

        # Create robot
        x = np.random.randint(0, WIDTH)
        y = np.random.randint(0, HEIGHT)
        self.create_robot(x, y, 0.0)

        # Record steps
        self.num_steps = 0

        # Create walls
        static = [
            pymunk.Segment(
                self.space.static_body,
                (0, 1), (0, HEIGHT), 1),
            pymunk.Segment(
                self.space.static_body,
                (1, HEIGHT), (WIDTH, HEIGHT), 1),
            pymunk.Segment(
                self.space.static_body,
                (WIDTH-1, HEIGHT), (WIDTH-1, 1), 1),
            pymunk.Segment(
                self.space.static_body,
                (1, 1), (WIDTH, 1), 1)
        ]
        for s in static:
            s.friction = 1.
            s.group = 1
            s.collision_type = 1
            s.color = (255,0,0)
        self.space.add(static)

        # Create obstacles

        self.obstacles = []

        num = np.random.randint(MIN_OBSTACLES, MAX_OBSTACLES)

        for n in range(0, num):
            type = np.random.randint(0,2)
            x = np.random.randint(0, WIDTH)
            y = np.random.randint(0, HEIGHT)
            r = np.random.randint(MIN_OBSTACLE_SIZE,MAX_OBSTACLE_SIZE)

            obstacle = self.create_obstacle(type, x, y, r)

            self.obstacles.append(obstacle)

        self.targets = []

        self.create_targets()

    # Functions:

    # Create robot

    def create_robot(self, x, y, r):
        self.robot_body = pymunk.Body(mass=1, moment=1)
        self.robot_body.position = x, y
        self.robot_shape = pymunk.Poly(self.robot_body, [(0.8 * ROBOT_SIZE * math.cos(math.pi/6), 0),(-0.2 * ROBOT_SIZE * math.cos(math.pi/6), -0.8 * ROBOT_SIZE * math.sin(math.pi/6)),(-0.2 * ROBOT_SIZE * math.cos(math.pi/6), 0.8 * ROBOT_SIZE * math.sin(math.pi/6))])

        self.robot_shape.color = (0,255,0)
        self.robot_shape.elasticity = 1.0
        self.robot_body.angle = r
        move_direction = Vec2d(1, 0).rotated(self.robot_body.angle)
        self.robot_body.apply_impulse_at_local_point(move_direction)
        self.space.add(self.robot_body, self.robot_shape)

    # Create target

    def create_targets(self):

        t = 0

        while t < TARGETS:
            x = np.random.randint(0, WIDTH)
            y = np.random.randint(0, HEIGHT)
            target = (x, y)

            collision = False
            for obstacle in self.obstacles:
                tar_sprite = pygame.sprite.Sprite()
                tar_sprite.radius = THRESHOLD
                tar_sprite.position = x, y
                tar_sprite.rect = pygame.Rect(x-THRESHOLD, y-THRESHOLD, THRESHOLD, THRESHOLD)

                obs_sprite = pygame.sprite.Sprite()
                obs_sprite.radius = obstacle.mass
                obs_sprite.position = obstacle.position
                obs_sprite.rect = pygame.Rect(obstacle.position[0]-obstacle.mass, obstacle.position[1]-obstacle.mass, 2*obstacle.mass, 2*obstacle.mass)

                if pygame.sprite.collide_circle(tar_sprite, obs_sprite) == True:
                    collision = True

            if collision != True:
                self.targets.append(target)
                t = t + 1
            #pygame.draw.circle(screen, (255, 0, 0), (x, y), 10)


    # Create obstacle

    def create_obstacle(self, type, x, y, r):
        c_body = pymunk.Body(mass=r, moment=r)
        if type == 0:
            c_shape = pymunk.Circle(c_body, r)
        else:
            c_shape = pymunk.Poly(c_body, [(-r, -r), (-r, r), (r, r), (r, -r)])
        c_shape.elasticity = 0.0
        c_body.position = x, y
        c_shape.color = (0,0,255)
        self.space.add(c_body, c_shape)
        return c_body

    def step(self, action):

        # Update screen and stuff.

        screen.fill((0,0,0))
        draw = DrawOptions(screen)
        self.space.debug_draw(draw)

        # Robot sensing

        x, y = self.robot_body.position
        readings = self.get_sonar_readings(x, y, self.robot_body.angle)
        normalized_readings = []
        for reading in readings:
            normalized_readings.append((reading - 20.0) / 20)
        state = np.array([normalized_readings])

        # Robot intelligence


        # Robot moving

        if action == 0:  # Turn left.
            self.robot_body.angle -= .2
        elif action == 1:  # Turn right.
            self.robot_body.angle += .2

        driving_direction = Vec2d(1, 0).rotated(self.robot_body.angle)
        self.robot_body.velocity = 100 * driving_direction

        # Update scenario

        self.move_obstacles()

        self.checkTargets()

        # Set the reward.
        # Car crashed when any reading == 1
        if self.car_is_crashed(readings):
            self.crashed = True
            reward = -500
            self.recover_from_crash(driving_direction)
        else:
            # Higher readings are better, so return the sum.
            reward = -5 + int(self.sum_readings(readings) / 10)
        self.num_steps += 1

        self.space.step(1./10)
        if draw_screen:
            pygame.display.flip()
        clock.tick()


        return reward, state

    def move_obstacles(self):
        # Randomly move obstacles around.
        ind = 1
        for obstacle in self.obstacles:
            if ind > np.size(self.obstacles, axis=0)/2:
                speed = random.randint(MIN_OBSTACLE_SPEED, MAX_OBSTACLE_SPEED)
                direction = Vec2d(1, 0).rotated(self.robot_body.angle + random.randint(-2, 2))
                obstacle.velocity = speed * direction
            ind = ind + 1

    def distance(self, p1, p2):
        return math.sqrt(math.pow(p2[0] - p1[0], 2) + math.pow(p2[1] - p1[1], 2))

    def checkTargets(self):
        xr = self.robot_body.position[0]
        yr = HEIGHT - self.robot_body.position[1]
        pr = (xr, yr)
        for target in self.targets:
            xt = target[0]
            yt = target[1]
            pt = (xt, yt)
            pygame.draw.circle(screen, (255, 0, 0), [xt, yt], 10)
            if self.distance(pr, pt) < THRESHOLD:
                self.targets.remove(target)
        if len(self.targets) == 0:
            self.create_targets()

    def car_is_crashed(self, readings):
        if readings[0] == 1 or readings[1] == 1 or readings[2] == 1 or readings[3] == 1 or readings[4] == 1 or readings[6] == 1 or readings[7] == 1:
            return True
        else:
            return False

    def recover_from_crash(self, driving_direction):
        """
        We hit something, so recover.
        """
        while self.crashed:
            # Teleport!
            x = np.random.randint(0, WIDTH)
            y = np.random.randint(0, HEIGHT)

            self.robot_body.position = (x, y)

            #self.robot_body.velocity = -100 * driving_direction
            self.crashed = False


            #for i in range(10):
            #    self.robot_body.angle += .2  # Turn a little.
                #screen.fill(pygame.Color('gray7'))  # Red is scary!
            #    screen.fill((128,128,128))  # Red is scary!
            draw = DrawOptions(screen) #, self.space)
            self.space.debug_draw(draw)
            #    self.space.step(1./10)
            #    if draw_screen:
            #        pygame.display.flip()
            #    clock.tick()

    def sum_readings(self, readings):
        """Sum the number of non-zero readings."""
        tot = 0
        for i in readings:
            tot += i
        return tot

    def get_sonar_readings(self, x, y, angle):
        readings = []
        # Make our arms.
        arm_back = self.make_sonar_arm(x, y)
        arm_left_back = arm_back
        arm_left = arm_back
        arm_left_front = arm_back
        arm_front = arm_back
        arm_right_front = arm_back
        arm_right = arm_back
        arm_right_back = arm_back

        # Rotate them and get readings.
        readings.append(self.get_arm_distance(arm_back, x, y, angle, math.pi))
        readings.append(self.get_arm_distance(arm_left_back, x, y, angle, 3*math.pi/4))
        readings.append(self.get_arm_distance(arm_left, x, y, angle, math.pi/2))
        readings.append(self.get_arm_distance(arm_left_front, x, y, angle, math.pi/4))
        readings.append(self.get_arm_distance(arm_front, x, y, angle, 0))
        readings.append(self.get_arm_distance(arm_right_front, x, y, angle, -math.pi/4))
        readings.append(self.get_arm_distance(arm_right, x, y, angle, -math.pi/2))
        readings.append(self.get_arm_distance(arm_right_back, x, y, angle, -3*math.pi/4))

        if show_sensors:
            pygame.display.update()

        return readings

    def get_arm_distance(self, arm, x, y, angle, offset):
        # Used to count the distance.
        i = 0

        # Look at each point and see if we've hit something.
        for point in arm:
            i += 1

            # Move the point to the right spot.
            rotated_p = self.get_rotated_point(
                x, y, point[0], point[1], angle + offset
            )

            # Check if we've hit something. Return the current i (distance)
            # if we did.
            if rotated_p[0] <= 0 or rotated_p[1] <= 0 \
                    or rotated_p[0] >= WIDTH or rotated_p[1] >= HEIGHT:
                return i  # Sensor is off the screen.
            else:
                obs = screen.get_at(rotated_p)
                if self.get_track_or_not(obs) != 0:
                    return i

            if show_sensors:
                pygame.draw.circle(screen, (255, 255, 255), (rotated_p), 2)

        return i

    def make_sonar_arm(self, x, y):
        spread = 10  # Default spread.
        distance = 20  # Gap before first sensor.
        arm_points = []

        for i in range(1, 40):
            arm_points.append((distance + x + (spread * i), y))

        return arm_points

    def get_rotated_point(self, x_1, y_1, x_2, y_2, radians):
        # Rotate x_2, y_2 around x_1, y_1 by angle.
        x_change = (x_2 - x_1) * math.cos(radians) + \
            (y_2 - y_1) * math.sin(radians)
        y_change = (y_1 - y_2) * math.cos(radians) - \
            (x_1 - x_2) * math.sin(radians)
        new_x = x_change + x_1
        new_y = HEIGHT - (y_change + y_1)
        return int(new_x), int(new_y)

    def get_track_or_not(self, reading):
        if reading == (0, 0, 0):
            return 0
        else:
            return 1

if __name__ == "__main__":
    game_state = GameState()
    while True:
        game_state.step((random.randint(0, 2)))
        pygame.time.wait(PAUSE)