# Classes for agents dynamics

import numpy as np
from pyib2c.Tools.Agents.PoseClasses import PoseV1Class as PoseClass


class DynV1Class:

    #   2D movement
    #   0-order dynamics

    def __init__(self, pose_0):
        self.pose = PoseClass
        self.pose = pose_0
        self.t_last = 0

    def update_pose(self, V_c, t):
        self.pose.V = V_c
        self.pose.X = self.pose.X + V_c*(t-self.t_last)
        self.t_last = t

        return self.pose


class DynV2Class:
    #   2D movement
    #   1-order dynamics

    def __init__(self, pose_0, k=1, tau_s=1):

        self.pose = pose_0
        self.t_last = 0

        self.k = k
        self.tau_s = tau_s

    def update_pose(self, V_c, t):

        delta_t = t - self.t_last

        for i in range(2):
            self.pose.V[i] = self.k*V_c[i]*(1-np.exp(-delta_t/self.tau_s))+self.pose.V[i]*np.exp(-delta_t/self.tau_s)

        self.pose.X = self.pose.X + self.pose.V*(t-self.t_last)
        self.pose.psi = np.arctan2(self.pose.V[0], self.pose.V[1])

        self.t_last = t

        return self.pose
