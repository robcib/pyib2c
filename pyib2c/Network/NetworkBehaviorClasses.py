import numpy as np
from tabulate import tabulate
import os
import inspect
import warnings
import pickle

from pyib2c.Behaviors.FusionBehaviorClass import FusionBehaviorClass
import xml.etree.cElementTree as ET

from pydoc import locate


class NetworkBehaviorClass:

    def __init__(self, ID=1):

        # Identification of the network
        self.ID = ID

        # Fusion dictionary: fixed function, activity and rating is considered
        self.fusion_dictionary = ['Max', 'Weighted', 'Weighted_max']

        # Behaviors dictionary
        self.behaviors_dictionary = []
        self.generate_behaviors_dictionary()

        # Get input and output types from the classes and add to the dictionaries
        self.get_input_output_info()

        # Output and input information
        self.output_info = []
        self.input_info = []

        # Functionality of the network
        self.inputs_network = 0
        self.stimulations_network = np.zeros(1)
        self.inhibitions_network = np.zeros(1)
        self.output = 0
        self.activity = 0
        self.rating = 0

        self.output_definition = []

        self.execution_order = []
        self.execution_array = []
        self.n_modules = int(0)

        # Creation of the dictionaries
        self.fusion_list = {self.fusion_dictionary[i]: 0 for i in range(np.size(self.fusion_dictionary))}
        self.behaviors_list = {self.behaviors_dictionary[i][0]: 0 for i in range(np.size(self.behaviors_dictionary, axis=0))}
        self.external_behaviors = np.array([])

        # Matrices of connections
        self.I_O_conn = []
        self.Stimulation_conn = []
        self.Inhibition_conn = []

        # List of modules references
        self.module_references = np.array(['Module', 'Type', 'ID', 'ID_Type'], dtype=np.object)
        temp = np.array(['Input_network', '-', 1, 1], dtype=np.object)
        self.module_references = np.vstack([self.module_references, temp])

        # Possible modules
        self.fusion_modules = np.ndarray((np.size(self.fusion_dictionary),), dtype=np.object)
        self.behavior_modules = np.ndarray((np.size(self.behaviors_dictionary, axis=0),), dtype=np.object)

    def generate_behaviors_dictionary(self):

        ##############################################################################################
        # It reads the available behaviors from behaviors_dictionary.xml, located in the same path
        #    than this module
        ##############################################################################################

        file_dictionary = os.path.join(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))), '..', 'Behaviors') \
                          + '/behaviors_dictionary.xml'
        tree = ET.ElementTree(file=file_dictionary)
        root = tree.getroot()

        self.behaviors_dictionary = np.zeros([np.size(root, axis=0), 3], dtype=object)
        i = 0
        for child in root:
            self.behaviors_dictionary[i, 0] = child.get('name')
            self.behaviors_dictionary[i, 1] = child.find('class_name').text
            self.behaviors_dictionary[i, 2] = child.find('module_name').text
            i += 1

    def get_input_output_info(self):

        ##############################################################################################
        # This function retrieves the information about the inputs and outputs of each behavior module
        # The information is shown and used afterwards to check if the connections are correct
        ##############################################################################################

        # Transform behaviors_dictionary into array with two more columns with the io info
        self.behaviors_dictionary = np.array(self.behaviors_dictionary, dtype=object)
        n_behaviors = np.size(self.behaviors_dictionary, axis=0)
        self.behaviors_dictionary = np.concatenate((self.behaviors_dictionary, np.zeros([n_behaviors, 2],
                                                                                        dtype=object)), axis=1)

        # Load each class and add the input and the output information to the dictionary
        for i in range(np.size(self.behaviors_dictionary[:, 0], axis=0)):
            name_behavior = 'pyib2c.Behaviors.' + self.behaviors_dictionary[i, 2] + '.' + \
                                self.behaviors_dictionary[i, 1]
            this_class = locate(name_behavior)
            if this_class is None:
                warning_msg = 'Behavior ' + self.behaviors_dictionary[i][1] + ' badly defined. Please, check behavior ' \
                                                                            'list or its definition in the network'
                warnings.warn(warning_msg)
            else:
                this_class = this_class()
                self.behaviors_dictionary[i][3] = this_class.input_info
                self.behaviors_dictionary[i][4] = this_class.output_info

        # Show information
        print("\n")
        print("--- Available behaviors ---")
        print(tabulate(self.behaviors_dictionary))
        print("\n")

    def add_external_behavior(self, name, path):

        ##############################################################################################
        # Adds the name and the path of the external behavior in a list, to be added afterwards
        ##############################################################################################

        if np.size(self.external_behaviors) == 0:
            self.external_behaviors = np.array([name, path])
        else:
            self.external_behaviors = np.vstack([self.external_behaviors, np.array([name, path])])

    def generate(self):

        ##############################################################################################
        # Having specified the number of each module, it creates them inside the network
        # It associates each module to an ID, creates the connection matrices and show the information
        ##############################################################################################

        # Initialize output
        self.output = self.output_info['Size']

        # Get the number of fusion modules specified by the user
        n_fusion = 0
        for i in range(np.size(self.fusion_dictionary)):
            n_fusion += self.fusion_list[self.fusion_dictionary[i]]

        # Get the number of behavior modules specified by the user
        n_behaviors = 0
        for i in range(np.size(self.behaviors_dictionary, axis=0)):
            n_behaviors += self.behaviors_list[self.behaviors_dictionary[i][0]]

        # Add number of external behaviors
        if np.size(np.shape(self.external_behaviors)) > 1:
            n_behaviors += np.size(self.external_behaviors, axis=0)
        elif np.size(np.shape(self.external_behaviors)) == 1 and np.size(self.external_behaviors) > 0:
            n_behaviors += 1

        # ------------------------------------
        # Create the references of the modules
        # ------------------------------------
        ID = 2
        # Fusion modules
        for i in range(np.size(self.fusion_dictionary)):
            ID_Type = 1
            for j in range(self.fusion_list[self.fusion_dictionary[i]]):
                temp = np.array(['Fusion', self.fusion_dictionary[i], ID, ID_Type], dtype=np.object)
                self.module_references = np.vstack([self.module_references, temp])
                ID += 1
                ID_Type += 1

        # Fusion behavior modules
        for i in range(np.size(self.behaviors_dictionary, axis=0)):
            ID_Type = 1
            for j in range(self.behaviors_list[self.behaviors_dictionary[i][0]]):
                temp = np.array(['Behavior', self.behaviors_dictionary[i][0], ID, ID_Type], dtype=np.object)
                self.module_references = np.vstack([self.module_references, temp])
                ID += 1
                ID_Type += 1

        # External behavior modules
        ID_Type = 1
        if np.size(self.external_behaviors.shape) > 1:
            for i in range(np.size(self.external_behaviors, axis=0)):
                temp = np.array(['External', self.external_behaviors[i, 0], ID, ID_Type], dtype=np.object)
                self.module_references = np.vstack([self.module_references, temp])
                ID += 1
                ID_Type += 1
        elif np.size(self.external_behaviors.shape) == 1 and np.size(self.external_behaviors) != 0:
            temp = np.array(['External', self.external_behaviors[0], ID, ID_Type], dtype=np.object)
            self.module_references = np.vstack([self.module_references, temp])
            ID += 1
            ID_Type += 1

        # Last: add the output reference
        temp = np.array(['Output_network', '-', ID, 1], dtype=np.object)
        self.module_references = np.vstack([self.module_references, temp])
        # ------------------------------------

        # Generate matrices
        self.n_modules = n_fusion + n_behaviors
        self.execution_order = np.zeros(self.n_modules+2, dtype=int)
        self.execution_array = np.zeros([self.n_modules+1, 5], dtype=object)
        self.I_O_conn = np.zeros([self.n_modules+2, self.n_modules+2], dtype=object)
        self.Stimulation_conn = -1*np.ones([self.n_modules+2, self.n_modules+2], dtype=object)
        self.Inhibition_conn = -1*np.ones([self.n_modules+2, self.n_modules+2], dtype=object)

        # Print information
        print('------------------')
        print('Network definition')
        print('------------------')
        print(tabulate(self.module_references))

    def add_connection(self, type, ID_out, ID_in=0, list_variables=-1):

        ##############################################################################################
        # This function adds the connections specified by the user
        # It completes each connection matrix and prints the result for confirmation
        # The list of variables to connect should be specified; otherwise, all will be passed (=-1)
        ##############################################################################################

        # Write connection in the matrices and record list of variables
        if type == 'I_O':
            self.I_O_conn[ID_out-1, ID_in-1] = list_variables
        elif type == 'Stimulation':
            if list_variables == -1:
                self.Stimulation_conn[ID_out-1, ID_in-1] = 0
            else:
                self.Stimulation_conn[ID_out - 1, ID_in - 1] = list_variables
        elif type == 'Inhibition':
            if list_variables == -1:
                self.Inhibition_conn[ID_out-1, ID_in-1] = 0
            else:
                self.Inhibition_conn[ID_out - 1, ID_in - 1] = list_variables
        elif type == 'Activity_Network':
            ID_in = self.n_modules+2
            self.Stimulation_conn[ID_out - 1, ID_in - 1] = int(0)
        elif type == 'Rating_Network':
            ID_in = self.n_modules+2
            self.Inhibition_conn[ID_out - 1, ID_in - 1] = int(0)

        print('Connected! From ID ', ID_out, ' to ID ', ID_in, ' in the ', type, ' port.')

    def finish_network(self):

        ##############################################################################################
        # Once the network has been completely specified, this function finishes it
        # First, it calculates the execution order
        # Second, it fills the execution array, which contains the classes and the variables to be
        #     passed between them
        ##############################################################################################

        # Calculate execution order
        self.calc_execution_order()

        # --------------------------------------------------------------------
        # Fill the execution matrix with the ID and the classes to be executed
        # --------------------------------------------------------------------
        for pos_execution in range(0, self.n_modules+1):

            this_ID = self.execution_order[pos_execution+1]  # ID of the module for which the input is retrieved

            # Store ID of module
            self.execution_array[this_ID-2, 0] = this_ID

            # Find which module corresponds to this ID
            this_module_type = self.module_references[this_ID, 0]  # Type of module
            this_module_name = self.module_references[this_ID, 1]  # Name of the module
            this_module_ID_type = self.module_references[this_ID, 3]

            # Fill the matrix with the module to be executed
            if this_module_type == 'Fusion':
                self.execution_array[pos_execution, 1] = FusionBehaviorClass(this_module_name)
            elif this_module_type == 'Behavior':
                pos = np.where(self.behaviors_dictionary[:, 0] == this_module_name)
                pos = pos[0]
                name_behavior = 'pyib2c.Behaviors.' + self.behaviors_dictionary[pos, 2] + \
                                '.' + self.behaviors_dictionary[pos, 1]
                this_class = locate(name_behavior[0])
                self.execution_array[pos_execution, 1] = this_class(this_module_ID_type)
            elif this_module_type == 'External':
                ID_type = self.module_references[np.where(this_ID == self.module_references[:, 2])[0][0], 3]
                if np.size(self.external_behaviors.shape) > 1:
                    name = self.external_behaviors[ID_type-1, 0]
                    path = self.external_behaviors[ID_type-1, 1]
                else:
                    name = self.external_behaviors[0]
                    path = self.external_behaviors[1]
                self.execution_array[pos_execution, 1] = self.load_external_behavior(name, path, ID_type)
            else:
                self.execution_array[pos_execution, 1] = 'Output'

        # --------------------------------------------------------------------

        # -----------------------------------------
        # Fill the array with the input information
        # -----------------------------------------

        for pos_execution in range(0, self.n_modules + 1):

            this_ID = self.execution_order[pos_execution + 1]  # ID of the module for which the input is retrieved
            this_module_type = self.module_references[this_ID, 0]

            # --------------------------------------
            # Find the inputs needed for this module
            # --------------------------------------
            # First, get how many outputs are needed
            if self.execution_array[pos_execution][1] == 'Output':
                this_ID_input_info = self.output_info
            elif this_module_type == 'Fusion':
                self.execution_array[pos_execution][1].update_IO_info(self, this_ID)  # The input info must be generated
                this_ID_input_info = self.execution_array[pos_execution][1].input_info
            else:
                this_ID_input_info = self.execution_array[pos_execution][1].input_info

            n_inputs = np.size(this_ID_input_info['Size'])
            self.execution_array[pos_execution, 2] = np.zeros((2, n_inputs), dtype=int)  # [ID module source][Variable]

            # Fill the inputs information
            count = 0  # Only used for filling fuse modules
            for input_source_ID in range(1, self.n_modules+2):

                inputs = self.I_O_conn[input_source_ID-1, this_ID-1]

                if inputs != 0 and inputs != -1:  # Add the corresponding inputs
                    for j in range(0, np.size(inputs, axis=0)):
                        variables_position_source = inputs[j][0]
                        variable_position = inputs[j][1]
                        self.execution_array[pos_execution, 2][0][variable_position] = input_source_ID
                        self.execution_array[pos_execution, 2][1][variable_position] = variables_position_source
                elif inputs == -1:  # Add all the inputs, substitute array by -1 and stop loop
                    if this_module_type == 'Fusion':
                        self.execution_array[pos_execution, 2][0][count] = input_source_ID
                        self.execution_array[pos_execution, 2][1][count] = -1
                        count += 1
                    else:  # If it is not a fusion module, it must be the only one input. Break the loop
                        self.execution_array[pos_execution, 2] = np.array([[input_source_ID], [-1]])
                        break
            # --------------------------------------

            # --------------------------------------
            # Find the stimulations for this module
            # --------------------------------------
            # Count how many stimulations there are
            n_stimulations = 0
            for input_source_ID in range(1, self.n_modules + 2):
                if self.Stimulation_conn[input_source_ID-1, this_ID-1] != -1:
                    n_stimulations += 1

            # Store in stimulations inputs
            if n_stimulations > 0:
                self.execution_array[pos_execution, 3] = np.zeros((2, n_stimulations), dtype=int)  # [ID module source]
                pos = 0
                for input_source_ID in range(1, self.n_modules + 2):
                    if self.Stimulation_conn[input_source_ID-1, this_ID-1] != -1:
                        self.execution_array[pos_execution, 3][0][pos] = input_source_ID
                        self.execution_array[pos_execution, 3][1][pos] = self.Stimulation_conn[input_source_ID-1, this_ID-1]
                        pos += 1
            # --------------------------------------

            # --------------------------------------
            # Find the inhibitions for this module
            # --------------------------------------
            # Count how many inhibitions there are
            n_inhibitions = 0
            for input_source_ID in range(1, self.n_modules + 2):
                if self.Inhibition_conn[input_source_ID-1, this_ID-1] != -1:
                    n_inhibitions += 1

            # Store in inhibitions inputs
            if n_inhibitions > 0:
                self.execution_array[pos_execution, 4] = np.zeros((2, n_inhibitions), dtype=int)  # [ID module source]
                pos = 0
                for input_source_ID in range(1, self.n_modules + 2):
                    if self.Inhibition_conn[input_source_ID-1, this_ID-1] != -1:
                        self.execution_array[pos_execution, 4][0][pos] = input_source_ID
                        self.execution_array[pos_execution, 4][1][pos] = self.Inhibition_conn[input_source_ID-1, this_ID-1]
                        pos += 1
            # --------------------------------------

        # -----------------------------------------

        # Check if the inputs have been properly connected
        self.check_input_connections()
        print('Network created properly')

    def calc_execution_order(self):

        ##############################################################################################
        # This function assigns an execution order to each module inside the net
        # TEMPORARY SOLUTION: IN FUTURE VERSIONS, THIS WILL BE IMPROVED
        ##############################################################################################

        # Input
        self.execution_order[0] = 1
        # Output
        self.execution_order[-1] = self.n_modules+2

        # Rest of the modules
        for i in range(1, self.n_modules+1):
            # TEMPORARY SOLUTION: order by ID
            self.execution_order[i] = i+1

    def check_input_connections(self):

        ##############################################################################################
        # This function checks whether the inputs have been properly connected
        ##############################################################################################

        for pos_execution in range(self.n_modules+1):

            this_ID = self.execution_order[pos_execution+1]  # ID of the module for which the input is retrieved

            # If the module to be checked is a fusion, skip it. It was checked when defining its inputs/outputs
            if self.module_references[np.where(this_ID == self.module_references[:, 2]), 0] == 'Fusion':
                continue

            if self.execution_array[pos_execution][1] == 'Output':
                this_ID_input_info = self.output_info
            else:
                this_ID_input_info = self.execution_array[pos_execution, 1].input_info
            n_inputs = np.size(this_ID_input_info['Size'], axis=0)

            # Check the first element to see if it is equal to -1 (all variables passed)
            pos_var_source = self.execution_array[pos_execution, 2][1][0]
            if pos_var_source == -1:
                n_inputs = 1

            # -------------------
            # Check each variable
            # -------------------
            for pos_var in range(n_inputs):
                # Find out the ID of the source module and get the information
                ID_source = self.execution_array[pos_execution, 2][0][pos_var]  # ID of the source module
                pos_var_source = self.execution_array[pos_execution, 2][1][pos_var]  # Pos of the variable at source
                if ID_source == 1:
                    source_output_info = self.input_info
                elif ID_source == 0:
                    error_msg = 'Network ERROR: the input of ID ' + str(this_ID) + ' has not been connected'
                    raise RuntimeError(error_msg)
                else:
                    pos_ID_source = np.where(ID_source == self.execution_array[:, 0])[0][0]
                    source_output_info = self.execution_array[pos_ID_source, 1].output_info

                # -------------------------------
                # Check if the sizes are the same
                # -------------------------------
                error_msg = ''
                if pos_var_source == -1:  # Compare all the variables
                    n_vars_input = np.size(this_ID_input_info['Size'], axis=0)
                    n_vars_output = np.size(source_output_info['Size'], axis=0)

                    if n_vars_input != n_vars_output:
                        error_msg = 'Network ERROR: bad connection between ID ' + str(ID_source) + ' and ID ' + \
                                    str(this_ID) + '. Different number of variables.'
                        raise RuntimeError(error_msg)

                    for pos_var in range(n_vars_input):

                        if hasattr(this_ID_input_info['Size'][pos_var], '__module__') or \
                                hasattr(source_output_info['Size'][pos_var], '__module__'):  # The var is a class

                            if type(this_ID_input_info['Size'][pos_var]) != \
                                    type(source_output_info['Size'][pos_var]):
                                error_msg = 'Network ERROR: bad connection between ID ' + str(ID_source) + ' and ID ' + \
                                            str(this_ID) + ', from variable ' + str(pos_var_source) + \
                                            ' to variable ' + str(pos_var)

                        else:  # It must be then an array

                            if this_ID_input_info['Size'][pos_var].size != source_output_info['Size'][pos_var].size:
                                error_msg = 'Network ERROR: bad connection between ID ' + str(ID_source) + ' and ID ' + \
                                            str(this_ID) + ', from variable ' + str(pos_var_source) + \
                                            ' to variable ' + str(pos_var)

                else:  # Compare only one specific variable

                    if hasattr(this_ID_input_info['Size'][pos_var], '__module__') or \
                            hasattr(source_output_info['Size'][pos_var_source], '__module__'):  # The var is a class

                        if type(this_ID_input_info['Size'][pos_var]) != type(source_output_info['Size'][pos_var_source]):
                            error_msg = 'Network ERROR: bad connection between ID ' + str(ID_source) + ' and ID ' + \
                                        str(this_ID) + ', from variable ' + str(pos_var_source) + \
                                        ' to variable ' + str(pos_var)

                    else:  # It must be then an array

                        if this_ID_input_info['Size'][pos_var].size != source_output_info['Size'][pos_var_source].size:
                            error_msg = 'Network ERROR: bad connection between ID ' + str(ID_source) + ' and ID ' + \
                                        str(this_ID) + ', from variable ' + str(pos_var_source) + ' to variable ' + \
                                        str(pos_var)

                if error_msg != '':
                    raise RuntimeError(error_msg)
                # -------------------------------
            # -------------------

    def assign_value_to_parameter(self, ID, parameter_name, value):

        ##############################################################################################
        # It assigns a value to a specific parameter of a module
        # The module is specified by the ID, and the parameter by the parameter_name argument
        ##############################################################################################

        pos_ID = np.where(ID == self.execution_array[:, 0])[0][0]
        if hasattr(self.execution_array[pos_ID, 1], parameter_name):
            setattr(self.execution_array[pos_ID, 1], parameter_name, value)
        else:
            error_msg = 'Parameter assignment ERROR: the parameter ' + parameter_name + ' does not exist in the ' + \
                        'module ID ' + str(ID)
            raise RuntimeError(error_msg)

    def call_behavior_method(self, ID, method_name, args=[]):

        ##############################################################################################
        # It call to a specific method of a behavior module
        ##############################################################################################

        pos_ID = np.where(ID == self.execution_array[:, 0])[0][0]
        if hasattr(self.execution_array[pos_ID, 1], method_name):
            if args == []:
                getattr(self.execution_array[pos_ID, 1], method_name)()
            else:
                getattr(self.execution_array[pos_ID, 1], method_name)(args)
        else:
            error_msg = 'Parameter assignment ERROR: the parameter ' + parameter_name + ' does not exist in the ' + \
                        'module ID ' + str(ID)
            raise RuntimeError(error_msg)

    def calc_outputs(self, inputs_network=np.zeros(0), stimulations_network=np.ones(1), inhibitions_network=np.zeros(1)):

        ##############################################################################################
        # It executes the net following the execution order, calculating the output and the rating
        ##############################################################################################

        self.inputs_network = inputs_network
        self.stimulations_network = stimulations_network
        self.inhibitions_network = inhibitions_network

        for pos_execution in range(self.n_modules+1):

            # Get inputs for this module
            inputs_module = self.retrieve_inputs(pos_execution)

            # Get stimulations for this module
            stimulations_module = self.retrieve_stimulations(pos_execution)

            # Get inhibitions for this module
            inhibitions_module = self.retrieve_inhibitions(pos_execution)

            # Update outputs of the module
            if self.execution_array[pos_execution, 1] == 'Output':
                self.output = inputs_module
                self.activity = stimulations_module  # For the output, instead of stimulation, connect to activity
                self.rating = inhibitions_module  # For the output, instaead of inhibition, pass rating

            else:
                self.execution_array[pos_execution, 1].calc_outputs(inputs_module, stimulations_module, inhibitions_module)

    def retrieve_inputs(self, pos_execution):

        ##############################################################################################
        # Retrieve the inputs for this module
        ##############################################################################################

        # Initialize the input array
        if self.execution_array[pos_execution][1] == 'Output':
            this_ID_input_info = self.output_info
        else:
            this_ID_input_info = self.execution_array[pos_execution, 1].input_info

        this_ID = self.execution_array[pos_execution, 0]
        this_type = self.module_references[this_ID, 0]

        n_inputs = np.size(this_ID_input_info['Size'])
        if this_type == 'Fusion':
            inputs_module = np.zeros([3, n_inputs], dtype=object)
        else:
            inputs_module = np.zeros(n_inputs, dtype=object)

        # Retrieve each variable from the modules
        for pos_var in range(n_inputs):
            ID_source = self.execution_array[pos_execution, 2][0][pos_var]  # ID of the source module
            pos_var_source = self.execution_array[pos_execution, 2][1][pos_var]  # Pos of the variable at source
            if ID_source == 1:
                output_source = self.inputs_network
                activity_source = self.stimulations_network
                rating_source = 1
            else:
                pos_ID_source = np.where(ID_source == self.execution_array[:, 0])[0][0]
                output_source = self.execution_array[pos_ID_source, 1].get_output()
                activity_source = self.execution_array[pos_ID_source, 1].get_activity()
                rating_source = self.execution_array[pos_ID_source, 1].get_rating()

            if pos_var_source == -1:  # Pass all the variables
                if this_type != 'Fusion':
                    return output_source
                else:  # For the fusion module, include the activity and the rating
                    inputs_module[0][pos_var] = output_source[0]
                    inputs_module[1][pos_var] = activity_source
                    inputs_module[2][pos_var] = rating_source

            else:
                if this_type != 'Fusion':
                    inputs_module[pos_var] = output_source[pos_var_source]
                else:
                    inputs_module[0][pos_var] = output_source[pos_var_source]
                    inputs_module[1][pos_var] = activity_source
                    inputs_module[2][pos_var] = rating_source

        return inputs_module

    def retrieve_stimulations(self, pos_execution):

        ##############################################################################################
        # Retrieve the stimulations for this module
        ##############################################################################################

        if not np.any(self.execution_array[pos_execution, 3] != 0):  # If there is a 0, the module is always stimulated
            return np.ones(1)

        # Initialize the stimulation array
        n_stimulations = np.size(self.execution_array[pos_execution, 3], axis=1)
        stimulations_module = np.zeros(n_stimulations)

        # Retrieve stimulation from the modules
        for pos_stimulation in range(n_stimulations):
            ID_source = self.execution_array[pos_execution, 3][0][pos_stimulation]  # ID of the source module
            pos_var_source = self.execution_array[pos_execution, 3][1][pos_stimulation]  # Position of the variable
            if ID_source == 1:
                stimulation_source = self.stimulations_network[pos_var_source]
            else:
                pos_ID_source = np.where(ID_source == self.execution_array[:, 0])[0][0]
                stimulation_source = self.execution_array[pos_ID_source, 1].get_activity()[pos_var_source]

            stimulations_module[pos_stimulation] = stimulation_source

        return stimulations_module

    def retrieve_inhibitions(self, pos_execution):

        ##############################################################################################
        # Retrieve the inhibitions for this module
        ##############################################################################################

        if not np.any(self.execution_array[pos_execution, 4] != 0):  # If there is a 0, the module is not inhibited
            return np.zeros(1)

        # Initialize the inhibition array
        n_inhibitions = np.size(self.execution_array[pos_execution, 4], axis=1)
        inhibitions_module = np.zeros(n_inhibitions)

        # Retrieve inhibitions from the modules
        for pos_inhibition in range(n_inhibitions):
            ID_source = self.execution_array[pos_execution, 4][0][pos_inhibition]  # ID of the source module
            pos_var_source = self.execution_array[pos_execution, 4][1][pos_inhibition]  # Position of the variable
            if ID_source == 1:
                inhibition_source = self.inhibitions_network[pos_var_source]
            else:
                pos_ID_source = np.where(ID_source == self.execution_array[:, 0])[0][0]
                if self.execution_array[pos_execution, 1] == 'Output':  # For the output, the rating is taken
                    inhibition_source = self.execution_array[pos_ID_source, 1].get_rating()[pos_var_source]
                else:
                    inhibition_source = self.execution_array[pos_ID_source, 1].get_activity()[pos_var_source]

            inhibitions_module[pos_inhibition] = inhibition_source

        return inhibitions_module

    def store_behavior_module(self, ID, path='~', name=''):

        ##############################################################################################
        # Given a specific ID, defined in this network, it stores it as a single file
        ##############################################################################################

        pos_ID = np.where(ID == self.execution_array[:, 0])[0][0]
        self.execution_array[pos_ID, 1].store_behavior_class(path=path, name=name)

    def store_network(self, path='~', name=''):

        ##############################################################################################
        # It stores the network as an object, to be loaded for other networks
        ##############################################################################################

        if name == '':
            name = self.__module__.split('.')[1] + '_ID' + str(self.ID) + '.ntm'
        else:
            name += '.ntm'

        # Store current directory and move to the user path
        current_path = os.getcwd()
        os.chdir(os.path.expanduser("~"))

        if not os.path.exists(path):
            os.makedirs(path)

        name_complete = path + '/' + name

        f = open(name_complete, 'wb')
        pickle.dump(self, f)
        f.close()

        # Return to the initial path
        os.chdir(current_path)

    def load_external_behavior(self, name, path, ID_type):

        ##############################################################################################
        # It loads a previously stored network and uses it as a behavior module
        ##############################################################################################

        # Store current directory and move to the user path
        current_path = os.getcwd()
        os.chdir(os.path.expanduser("~"))

        name_complete = path + '/' + name
        f = open(name_complete, 'rb')
        behavior = pickle.load(f)
        behavior.ID = ID_type
        f.close()

        # Return to the initial path
        os.chdir(current_path)

        return behavior

    def get_output(self):
        return self.output

    def get_activity(self):
        return self.activity

    def get_rating(self):
        return self.rating





