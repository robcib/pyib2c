import numpy as np

from pyib2c.Tools.Agents.PoseClasses import PoseV1Class
from pyib2c.Behaviors.BehaviorTemplateClass import BehaviorTemplateClass


class AvoidObstaclesBehaviorClass(BehaviorTemplateClass):

    def __init__(self, ID=1):

        # Output and input information
        # -------------------------------------------------------
        self.input_info = {'Size': np.array([PoseV1Class(),
                                             np.zeros([8])]),
                           'Type': np.array(['Pose', 'ProximitySensors']),
                           'Description': ['Pose of the agent',
                                           'Proximity measurements, 8 values']}

        self.output_info = {'Size': np.array([np.zeros([2])]),
                            'Type': 'Velocity2D',
                            'Description': '2D Velocity Command'}
        # -------------------------------------------------------

        # Initialize base behavior class
        BehaviorTemplateClass.__init__(self, ID)

        # Variables for this behavior
        self.v_n = 1
        self._sensors = np.zeros([8])
        self._angles = np.array([2*np.pi/8*i for i in range(8)])

    def calc_transfer(self, inputs):

        pose = inputs[0]
        self._sensors = inputs[1]

        # Vector creation
        V = np.zeros([2, 8])
        for i in range(8):
            v = (1-self._sensors[i])/(self._sensors[i]+0.1)
            psi = self._angles[i] + np.pi
            V[0, i] = v*np.sin(psi)
            V[1, i] = v * np.cos(psi)

        V_final = np.sum(V, axis=1)

        if np.linalg.norm(V_final) > 0:

            psi_final = np.arctan2(V_final[0], V_final[1])  # Refer to the agent, it must be rotated

            psi_final_rotated = psi_final + pose.psi
            psi_final_rotated = np.arctan2(np.sin(psi_final_rotated), np.cos(psi_final_rotated))

            # Normalized final vector
            V_final = self.v_n*np.array([np.sin(psi_final_rotated), np.cos(psi_final_rotated)])

        self.output[0] = V_final

    def calc_activity(self):

        self.activity = np.array([self.activation*(1 - np.min(self._sensors)), 0])

    def calc_rating(self):

        self.rating = np.array([np.min(self._sensors)])


