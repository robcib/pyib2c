import numpy as np
import pickle
import os


class BehaviorTemplateClass:

    def __init__(self, ID=1):

        self.ID = ID

        # Behavior variables
        self.output = self.output_info['Size']
        self.activity = 1
        self.activation = 1
        self.rating = 1

    def calc_outputs(self, inputs, stimulations=1, inhibitions=0):

        # Calculate a activation
        stimulations = np.clip(stimulations, 0, 1)
        inhibitions = np.clip(inhibitions, 0, 1)
        self.activation = np.max(stimulations)*(1-np.max(inhibitions))

        # Call each function defined in the upper class
        self.calc_activity()
        self.calc_transfer(inputs)
        self.calc_rating()

        return self.output, self.activity, self.rating

    def store_behavior_class(self, path='~', name=''):

        if name == '':
            name = self.__module__+ '_ID' + str(self.ID) + '.bm'  #  .split('.')[0]
        else:
            name += '.bm'

        # Store current directory and move to the user path
        current_path = os.getcwd()
        os.chdir(os.path.expanduser("~"))

        if not os.path.exists(path):
            os.makedirs(path)

        name_complete = path + '/' + name

        f = open(name_complete, 'wb')
        pickle.dump(self, f)
        f.close()

        # Return to the initial path
        os.chdir(current_path)

        print('Behavior stored with name: ' + name)

    def get_output(self):
        return self.output

    def get_activity(self):
        return self.activity

    def get_rating(self):
        return self.rating
