import numpy as np

from pyib2c.Tools.Agents.PoseClasses import PoseV1Class
from pyib2c.Behaviors.BehaviorTemplateClass import BehaviorTemplateClass


class DecreaseVelocityBehaviorClass(BehaviorTemplateClass):

    def __init__(self, ID=1):

        # Output and input information
        # -------------------------------------------------------
        self.output_info = {'Size': np.array([np.zeros([1])]),
                            'Type': 'Coefficient1D',
                            'Description': 'Velocity coefficient reduction'}
        self.input_info = {'Size': np.array([PoseV1Class(),
                                             np.zeros([1, 2])]),
                           'Type': np.array(['Pose', 'Position2D']),
                           'Description': ['Pose of the agent',
                                           '2D position of the target']}
        # -------------------------------------------------------

        # Initialize base behavior class
        BehaviorTemplateClass.__init__(self, ID)

        # Variables for this behavior
        self.P = 5
        self.v_n = 1
        self._delta_psi = 0

    def calc_transfer(self, inputs):

        # Get input variables
        pose = inputs[0]
        target_pos = inputs[1]

        # -------------------------------------------------------
        # Calc output vector with the transfer function
        # -------------------------------------------------------
        i_target = 0
        r_a_t = [target_pos[i_target][0] - pose.X[0], target_pos[i_target][1] - pose.X[1]]
        psi_a_t = np.arctan2(r_a_t[0], r_a_t[1])
        delta_psi = psi_a_t - pose.psi

        # Correction [-pi,+pi]
        self._delta_psi = np.arctan2(np.sin(delta_psi), np.cos(delta_psi))
        # -------------------------------------------------------

        alpha_v = 1 - np.abs(self._delta_psi)/np.pi

        self.output[0] = alpha_v

    def calc_activity(self):

        self.activity = np.array([self.activation])

    def calc_rating(self):

        self.rating = np.array([1 - np.abs(self._delta_psi) / np.pi])


