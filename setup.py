from setuptools import setup

setup(name='pyib2c',
      version='0.1',
      description='Python implementation of the iB2C architecture. Created for the SwarmCity project (CAR-UPM).',
      url='',
      author='Pablo Garcia-Aunon',
      author_email='pablo.garcia.aunon@upm.es',
      license='MIT',
      packages=['pyib2c'],
      zip_safe=False)
